from estoque import addEmvalortotal
from estoque import soma
from prettytable import PrettyTable
from repost import reporE
from repost import repor
resp = "S"

while resp == "S":
    nome = input("Informe o nome da mercadoria em estoque:")
    qmin = int(input("Informe a quantidade mínima dessa mercadoria:"))
    qatual = int(input("Informe a quantidade atual:"))
    qmax = int(input("Informe a quantidade máxima:"))

    if qatual > qmax:
        print("Você não pode receber uma quantidade maior do que a capacidade máxima.")
        break

    valuni = float(input("Informe o valor unitário:"))
    addEmvalortotal(qatual,valuni)
    repor(qatual,qmin,nome)
    if qmin <= 0 or qatual <= 0 or qmax <= 0:
        print("As informações devem ser maiores que 0.")
    resp = input("MAIS MERCADORIAS?(S/N)")
    if(resp == "N"):
        print("O valor total em estoque é de: ", soma())
        print(reporE())
