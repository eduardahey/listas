#6 - Interprete o seguinte código:

x = int(input("Digite um número: "))
y = int(input("digite um outro número: "))
z = (x*y)+5
if z <= 0:
    resposta = "A"
elif z <= 100:
    resposta = "B"
else:
    resposta = "C"
print("Resultado final: %i. Resposta: %s" %(z,resposta))

#Quais os valores serão armazenados em "z" e "resposta" para as seguintes situações:
#a) x = 5 e y = 10
#z=55 e resposta=B
#b) x = 200 e y = 100
#z=20005 e resposta=C
#c) x = 27 e y = -34
#z=-193 resposta=A
#d) x = -8 e y =40
#z=-315 resposta=A
#e) x = 50 e y = 3
#z=155 reesposta=C