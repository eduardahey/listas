inicio = int(input("Informe a hora de início do jogo:"))
termino = int(input("Informe a hora de término do jogo:"))
duracao = 0 
if 24 >= inicio > 0 and 24 >= termino > 0:
    if termino > inicio :
        duracao = termino-inicio
    else:
        duracao = termino + (24-inicio)
if duracao > 24:
    print("O tempo de jogo passou de 24 horas.")
else:
    print("O tempo do jogo é de %i" %(duracao))
    